<?php
namespace BadWolf\Bundle\RestBundle;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use BadWolf\Bundle\RestBundle\Definition\Mapping;
use BadWolf\Bundle\RestBundle\Endpoint;
use BadWolf\Bundle\RestBundle\Result;

/**
 * Provides extra helpers for dealing with endpoints that create content.
 */
abstract class ContentEndpoint extends Endpoint
{
    public function useTransaction()
    {
        return true;
    }

     /**
     * Creates a new instance of a the type stored in the mapping data.
     *
     * @param Form $form
     *
     * @return object|null Document
     */
    protected function createInstance(Form $form)
    {
        $instance = $this->getCreateModel()->apply('en', $form->getData());
        $instance->save();

        return $instance;
    }

    /**
     * Gets the class name that implements the item.
     *
     * @return string
     */
    public abstract function getContentClass();

    /**
     * Returns a new form type for this content.
     *
     * @return \Symfony\Component\Form\AbstractType
     */
    public function getContentFormType(Mapping $model)
    {
        return null;
    }

    public function handleCreate(Request $request)
    {
        // TODO: permissions

        $form = $this->createForm($this->getContentFormType($this->getCreateModel()));
        $form->handleRequest($request);

        $result = new Result();
        $result->addForm($form);

        if ($form->isValid() == false) {
            return $this->done($result, 400);
        } else {
            // check for a duplicate record
            if ($this->hasDuplicate($request, $form) == true) {
                return $this->abort('An item with this name already exists', 409);
            } else {
                $con = \Propel::getConnection();

                if ($this->useTransaction() === true) {
                    $con->beginTransaction();
                }

                {
                    $instance = $this->createInstance($form);

                    if ($instance !== null) {
                        $this->onCreated($result, $form, $instance);
                    }
                }

                if ($this->useTransaction() === true) {
                    $con->commit();
                }

                $result->data = $instance->exportAll($this->getContext());
            }
        }

        return $this->done($result, 201);
    }

    public function handleRead(Request $request, $id = null)
    {
        $result       = new Result();
        $result->data = [];

        // if we have a value for $id then we skip the query builder
        if ($id !== null) {
            $instance = $this->findInstance($id);

            if ($instance !== null) {
                $result->data = $instance->export($this->getContext());
            }
        } else {
            // build data
            $q = $this->createQueryBuilder(true);

            foreach ($q->find() as $obj) {
                $result->data[] = $obj->export($this->getContext());
            }

            $result->count = sizeof($result->data);

            // build total
            $result->total = $this->createQueryBuilder(true, false)
                    ->select('COUNT(entity)')
                    ->clearGroupByColumns() // FIXME: this will return incorrect numbers for queries that use grouping
                    ->count();
        }

        return $this->done($result);
    }

    public function handleUpdate(Request $request, $id)
    {
        // TODO: permissions

        $form = $this->createForm($this->getContentFormType($this->getUpdateModel()), null, ['method' => 'PUT']);
        $form->handleRequest($request);

        $result = new Result();
        $result->addForm($form);
        $statusCode = 200;

        if (($instance = $this->findInstance($id)) === null) {
            return $this->abort('An item with this key does not exist', 404);
        } else {
            $result->data = $instance->exportAll($this->getContext());

            if ($form->isValid() == true) {
                $con = \Propel::getConnection();
                $con->beginTransaction();
                {
                    $this->updateInstance($instance, $form);
                    $this->onUpdated($result, $form, $instance);
                }
                $con->commit();

                $result->data = $instance->exportAll($this->getContext());
            } else {
                $statusCode = 400;
            }
        }

        return $this->done($result, $statusCode);
    }

    /**
     * Find an instance from an ID.
     *
     * @param  mixed $id ID of the resource.
     *
     * @return mixed|null Content for the given ID or null.
     */
    protected function findInstance($id)
    {
        return $this->createQueryBuilder()->findOneByPrimaryKey($id);
    }

    /**
     * With the given form data, checks to see if an existing item already exists.
     *
     * @param Request $request
     * @param Form $form
     *
     * @return bool
     */
    protected function hasDuplicate(Request $request, Form $form)
    {
        return false;
    }

    /**
     * Called when a new instance of the content has been created.
     *
     * If you make changes to the model of the instance then you must call
     * save.
     *
     * @param Result $result Result to send to the client.  You can add your own data to it.
     * @param Form $form The form that was used to validate the data.
     * @param object $instance Instance that was created.
     */
    protected function onCreated(Result $result, Form $form, $instance)
    {

    }

    /**
     * Called when an instance of the content has been updated.
     *
     * If you make changes to the model of the instance then you must call
     * save.
     *
     * @param Result $result Result to send to the client.  You can add your own data to it.
     * @param Form $form The form that was used to validate the data.
     * @param object $instance Instance that was updated.
     */
    protected function onUpdated(Result $result, Form $form, $instance)
    {

    }

    /**
     * Creates a new instance of a the type stored in the mapping data.
     *
     * @param object $instance
     * @param Form $form
     *
     * @return object|null Document
     */
    protected function updateInstance($instance, Form $form)
    {
        $this
            ->getUpdateModel()
            ->apply($this->getRequest()->getLocale(), $form->getData(), $instance)
            ->save();

        return $instance;
    }
}
