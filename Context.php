<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle;

use BadWolf\Bundle\RestBundle\Definition\Parameter;

class Context
{

    private $name;

    private $version;

    private $tag;

    private $urlFormat;

    private $processedEnv;

    private $shouldProcessEnv;

    private $parameters = [
        'expansions' => [],
        'fields' => [],
        'filters' => []
    ];

    public function __construct($version, $name, $tag, $urlFormat = '/{version}/{tag}')
    {
        $this->name      = $name;
        $this->tag       = $tag;
        $this->version   = $version;
        $this->urlFormat = $urlFormat;
    }

    public function getFields()
    {
        $this->processEnvironment();

        return $this->parameters['fields'];
    }

    public function getFilter($name)
    {
        $this->processEnvironment();

        if ((is_array($this->parameters['filters']) == true) && (array_key_exists($name, $this->parameters['filters']) == true) && ($this->parameters['filters'][$name] !== null)) {
            return $this->parameters['filters'][$name];
        }

        return null;
    }

    public function setFields(array $fields)
    {
        $this->parameters['fields'] = $fields;
    }

    public function setFilter($name, $value)
    {
        $this->processEnvironment();

        if ((is_array($this->parameters['filters']) == true) && (array_key_exists($name, $this->parameters['filters']) == true) && ($this->parameters['filters'][$name] !== null)) {
            $this->parameters['filters'][$name] = $value;
        }
    }

    public function setShouldProcessEnvironment($value)
    {
        $this->shouldProcessEnv = $value;
    }

    public function getLimit()
    {
        return $this->getParameter('limit');
    }

    public function getName()
    {
        return $this->name;
    }

    public function getOffset()
    {
        return $this->getParameter('offset');
    }

    public function getParameter($key)
    {
        $this->processEnvironment();

        if (array_key_exists($key, $this->parameters) == true) {
            return $this->parameters[$key];
        }

        return null;
    }

    public function getStandardReadParameters()
    {
        $p = [
            new Parameter('expand',  'array',  [], 'List of sub-collections to expand.'),
            new Parameter('fields',  'string', '', 'List of fields to provide for each item.'),
            new Parameter('filters', 'array',  [], 'List of filters to apply.'),
            new Parameter('levels',  'int',    1,  'How many levels deep to travel for collections.'),
            new Parameter('limit',   'int',    50, 'How many items are to be fetched?'),
            new Parameter('offset',  'int',    0,  'At what offset should we start fetching items?')
        ];

        return $p;
    }

    public function getTag()
    {
        return $this->tag;
    }

    public function getUrl($url = '')
    {
        $u = str_replace(['{version}', '{tag}'], [$this->getVersion(), $this->getTag()], $this->urlFormat);

        if (mb_strlen($url) > 0) {
            $u .= '/' . $url;
        }

        return preg_replace('/\/{2,}/', '/', $u);
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function processFields($data = [])
    {
        foreach ($this->getStandardReadParameters() as $parameter) {
            $r = false;

            if (array_key_exists($parameter->getName(), $data) == true) {
                if (($parameter->getDefaultValue() == null) && (array_key_exists($parameter->getName(), $data) == false)) {
                    $r = false;
                } else {
                    // validate arrays here as builtin validation relies on strings
                    if ($parameter->expects('array') == true) {
                        if (is_array($data[$parameter->getName()]) == true) {
                            $r = true;
                        }
                    } else {
                        $r = preg_match($parameter->getValidatorPattern(), $data[$parameter->getName()]);
                    }
                }

                if (($r == 0) || ($r === false)) {
                    $app->abort(400, sprintf('Bad value for \'%s\', expected \'%s\'.', $parameter->getName(), $parameter->getType()));
                }

                if ($parameter->getName() == 'fields') {
                    $hasFields = true;
                }

                // we do some cheeky processing here to turn true/false in to booleans)
                $this->parameters[$parameter->getName()] = $this->processValue($data[$parameter->getName()]);
            } else {
                $this->parameters[$parameter->getName()] = $parameter->getDefaultValue();
            }
        }

        // fields comes as a comma separated list but we want it as an array
        if ($hasFields == true) {
            $this->parameters['fields'] = explode(',', $this->parameters['fields']);
        }

        $this->processedEnv = true;
    }

    private function processEnvironment()
    {
        if (($this->shouldProcessEnv === false) || ($this->processedEnv == true)) {
            return;
        }

        $data = $_GET;  // @todo: bring this out from the request

        $this->processFields($data);
    }

    public function reset()
    {
        $this->processedEnv = false;
    }

    public function toArray()
    {
        return [
            'format' => $this->urlFormat,
            'href' => $this->getUrl(),
            'name' => $this->name,
            'tag' => $this->tag,
            'version' => $this->version
        ];
    }

    public function wantsExpansion($name)
    {
        $this->processEnvironment();

        if (is_array($this->parameters['expand']) == true) {
            if ((array_key_exists('all', $this->parameters['expand']) == true) || (array_key_exists($name, $this->parameters['expand']) == true)) {
                return true;
            }
        }

        return false;
    }

    public function wantsField($name)
    {
        $this->processEnvironment();

        if (is_array($this->parameters['fields']) == false) {
            return false;
        }

        if (in_array('all', $this->parameters['fields']) == true) {
            return true;
        }

        return in_array($name, $this->parameters['fields']);
    }

    public function __sleep()
    {
        return [
            'urlFormat',
            'name',
            'tag',
            'version'
        ];
    }

    private function processValue($value)
    {
        if (is_array($value) == true) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->processValue($v);
            }
        } else
            if (is_string($value) == true) {
                if ($value === 'true') {
                    return true;
                } else {
                    if ($value === 'false') {
                        return false;
                    }
                }
            }

        return $value;
    }

    public static function fromArray(array $a)
    {
        return new Context($a['version'], $a['name'], $a['tag'], $a['format']);
    }
}
