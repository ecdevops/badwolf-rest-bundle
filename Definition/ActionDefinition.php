<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Definition;

use BadWolf\Bundle\RestBundle\Context;
use BadWolf\Bundle\RestBundle\Definition\Mapping;
use BadWolf\Bundle\RestBundle\Traits\ExportTrait;

class ActionDefinition
{
    use ExportTrait;

    private $definition;

    private $callable;

    private $description;

    private $isCreateAction = false;

    private $isUpdateAction = false;

    private $method;

    private $name;

    private $requiredPermission;

    private $summary;

    private $url;

    private $inputs = [];

    private $tokens = [];

    public static function getExportMapping()
    {
        return Mapping::create('Action Definition', __CLASS__)
        ->addField('description',         'string', 'getDescription',        null, false, 'Description of the endpoint.')
        ->addField('is_create_action',    'string', 'isCreateAction',        null, false, 'Is this the default action to call when creating a record?')
        ->addField('is_update_action',    'string', 'isUpdateAction',        null, false, 'Is this the default action to call when updating a record?')
        ->addField('method',              'string', 'getMethod',             null, false, 'HTTP verb to use when calling this action.')
        ->addField('name',                'string', 'getName',               null, false, 'Name of the module.')
        ->addField('required_permission', 'string', 'getRequiredPermission', null, false, 'Permission required to call this action.')
        ->addField('summary',             'string', 'getSummary',            null, false, 'Summary of the endpoint\'s purpose.');
    }

    public function __construct(Definition $definition, $name, $url, $method, $callable)
    {
        $this->callable   = $callable;
        $this->definition = $definition;
        $this->method     = mb_strtolower($method);
        $this->name       = $name;
        $this->url        = $url;
    }

    public function addToken($name, $type, $defaultValue, $description)
    {
        $this->tokens[$name] = new Parameter($name, $type, $defaultValue, $description);

        return $this;
    }

    public function addInput($name, $type, $defaultValue, $required, $description)
    {
        $this->inputs[$name] = new Parameter($name, $type, $defaultValue, $description, $required);

        return $this;
    }

    public function getCallable()
    {
        return $this->callable;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getHref(Context $context)
    {
        return $this->getUrl();
    }

    public function getInputs()
    {
        return $this->inputs;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRequiredPermission()
    {
        if ($this->requiredPermission !== null) {
            return $this->requiredPermission;
        }

        // @TODO: $this->requiredPermission = sprintf('ROLE_API_%s_%s_%s', Util::formatPermissionString($this->definition->getInitialContext()->getTag()), Util::formatPermissionString($this->definition->getName()), Util::formatPermissionString($this->getName()));

        return $this->requiredPermission;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function getTokens()
    {
        return $this->tokens;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function isCreateAction()
    {
        return $this->isCreateAction;
    }

    public function isUpdateAction()
    {
        return $this->isUpdateAction;
    }

    public function setCreateAction($value)
    {
        $this->isCreateAction = $value;

        return $this;
    }

    public function setUpdateAction($value)
    {
        $this->isUpdateAction = $value;

        return $this;
    }

    public function setDescription($value)
    {
        $this->description = $value;

        return $this;
    }

    public function setName($value)
    {
        $this->name = $value;

        return $this;
    }

    /**
     * Sets the input fields to match the given mapping.
     *
     * @param Mapping $mapping
     */
    public function setModelMapping(Mapping $mapping)
    {
        foreach ($mapping->getFields() as $field) {
            if ($field->isModel() == true) {
                $this->addInput($field->getName(), $field->getType(), null, $field->isRequired(), $field->getDescription());
            }
        }
        
        return $this;
    }

    public function setSummary($value)
    {
        $this->summary = $value;

        return $this;
    }
}
