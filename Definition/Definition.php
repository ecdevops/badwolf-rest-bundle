<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Definition;

use BadWolf\Bundle\RestBundle\Context;
use BadWolf\Bundle\RestBundle\Definition\Mapping;
use BadWolf\Bundle\RestBundle\Traits\ExportTrait;

class Definition
{

    use ExportTrait;

    private $description;

    private $initialContext;

    private $name;

    private $summary;

    private $actions = [];

    public function __construct($name, Context $initialContext)
    {
        $this->name = $name;
        $this->initialContext = $initialContext;
    }

    public static function getExportMapping()
    {
        return Mapping::create('Definition', __CLASS__)
            ->addField('actions',       'array',  null,             null, false, 'A controller can contain multiple actions (create, update, read, delete), these are the definitions.')
            ->addField('description',   'string', 'getDescription', null, false, 'Description of the endpoint.')
            ->addField('mapping',       'array',  null,             null, false, 'Holds information on how fields are mapped to backend values.')
            ->addField('name',          'string', 'getName',        null, false, 'Name of the module.')
            ->addField('provider_name', 'string', null,             null, false, 'Name of the provider that exposes this endpoint.')
            ->addField('provider_tag',  'string', null,             null, false, 'Tag of the provider that exposes this endpoint.')
            ->addField('summary',       'string', 'getSummary',     null, false, 'Summary of the endpoint\'s purpose.')

            ->addFilter('href',         'string', false, false, null,      'Find an endpoint with the given href.');
    }

    public function addAction($name, $path, $method, $callable)
    {
        return ($this->actions[] = new ActionDefinition($this, $name, $this->initialContext->getUrl($path), $method, $callable));
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getCreateAction()
    {
        foreach ($this->actions as $action) {
            if ($action->isCreateAction() == true) {
                return $action;
            }
        }

        return null;
    }

    public function getUpdateAction()
    {
        foreach ($this->actions as $action) {
            if ($action->isUpdateAction() == true) {
                return $action;
            }
        }

        return null;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getHref(Context $context)
    {
        $href = '';

        foreach ($this->actions as $action) {
            if (mb_stristr($action->getMethod(), 'get') !== false) {
                $href = $action->getUrl();
                break;
            }

            $href = $action->getUrl();
        }

        return $href;
    }

    public function getInitialContext()
    {
        return $this->initialContext;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getSummary()
    {
        return $this->summary;
    }

    public function setDescription($value)
    {
        $this->description = $value;

        return $this;
    }

    public function setSummary($value)
    {
        $this->summary = $value;

        return $this;
    }

    public static function create($name, Context $initialContext)
    {
        return new Definition($name, $initialContext);
    }
}
