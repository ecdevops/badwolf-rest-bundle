<?php

/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Definition;

class Field
{

    private $getter;
    private $setter;
    private $description;
    private $mapping;
    private $name;
    private $requiredPermission;
    private $type;
    private $isRequired;
    private $validationParameters;

    public function __construct(Mapping $mapping, $name, $getter, $setter, $isRequired, $description, $type, array $validationParameters = null)
    {
        $this->getter               = $getter;
        $this->setter               = $setter;
        $this->description          = $description;
        $this->mapping              = $mapping;
        $this->name                 = $name;
        $this->type                 = $type;
        $this->isRequired           = $isRequired;
        $this->validationParameters = $validationParameters;
    }

    /**
     * Gets the callback responsible for retrieving the value of this field.
     *
     * @return callable
     */
    public function getGetter()
    {
        return $this->getter;
    }

    /**
     * Gets the callback responsible for setting the value of this field.
     *
     * @return callable
     */
    public function getSetter()
    {
        return $this->setter;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getMapping()
    {
        return $this->mapping;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getRequiredPermission()
    {
        if ($this->requiredPermission !== null) {
            return $this->requiredPermission;
        }

        // @todo: $this->requiredPermission = sprintf('ROLE_DATA_%s_%s', Util::formatPermissionString($this->getMapping()->getDefiningClass()), Util::formatPermissionString($this->getName()));

        return $this->requiredPermission;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getValidationParameters()
    {
        return $this->validationParameters;
    }

    /**
     * Is this field part of the underlying model?
     *
     * @return boolean
     */
    public function isModel()
    {
        return ($this->getSetter() !== null);
    }

    /**
     * When this field is part of the model, must a value be provided?
     *
     *  @return boolean
     */
    public function isRequired()
    {
        return $this->isRequired;
    }
}
