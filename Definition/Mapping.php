<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Definition;

/**
 * Maps an external representation of a resource to an actual object.
 *
 * This is done through the use of getter and setter methods which exist on
 * the object.
 *
 * This allows for the creation of "virtual" resources that don't actually map
 * to any single object in the system.
 *
 * The fields added to a mapping are used to build the representation that is
 * sent to the client.
 *
 * For example:
 *
 * {
 * "name": "xyz"
 * "description": "a super description"
 * }
 */
class Mapping
{

    private $name;

    private $class;

    private $fields = [];

    private $filters = [];

    /**
     * Constructs a new mapping instance.
     *
     * @param string $name
     * @param string $class
     */
    public function __construct($name, $class)
    {
        $this->name  = $name;
        $this->class = $class;
    }

    /**
     * Adds a new field to the mapping.
     *
     * @param string $name
     *            The name of field. Used in the representation sent to the client.
     * @param string $type
     *            The data type of the field.
     * @param callable $getter
     *            Method to call on the resource to get the value.
     * @param callable $setter
     *            When applying a model, can this field be changed?
     * @param boolean $isRequired
     *            If this field has a setter, is it required when modifying this resource?
     * @param string $description
     *            Description of this field.
     * @param array $validationParameters
     *            List of validation settings.  These are symfony form settings.
     *
     * @return \BadWolf\Bundle\RestBundle\Definition\Mapping The mapping to allow chaining.
     */
    public function addField($name, $type, $getter, $setter, $isRequired, $description, array $validationParameters = null)
    {
        $this->fields[] = new Field($this, $name, $getter, $setter, $isRequired, $description, $type, $validationParameters);

        return $this;
    }

    public function addFilter($name, $type, $callable, $description)
    {
        $this->filters[] = new Filter($this, $name, $callable, $description, $type);

        return $this;
    }

    /**
     * Using the mapping information, applies the field data in $data
     * to $obj.
     *
     * @param string $locale
     *            Locale of the data to set.
     * @param array $data
     *            Data to apply.
     * @param object $obj
     *            Object to apply the mapping to. If this is null, a new
     *            instance is created.
     */
    public function apply($locale, array $data, $obj = null)
    {
        if ($obj === null) {
            $class = $this->getDefiningClass();
            $obj = new $class();
        }

        if (method_exists($obj, 'setLocale') === true) {
            $obj->setLocale($locale);
        }

        // TODO: should we throw an exception when data is supplied for a field
        // that cannot be set?
        foreach ($data as $key => $value) {
            if (($field = $this->findField($key)) !== null) {
                if ($field->isModel() == true) {
                    $setter = $field->getSetter();
                    $obj->$setter($value);
                }
            }
        }

        return $obj;
    }

    public function findExpansion($name)
    {
        // until we make expansions a proper feature, we treat them as fields
        return $this->findField($name);
    }

    public function findField($name)
    {
        foreach ($this->fields as $field) {
            if (strcasecmp($name, $field->getName()) == 0) {
                return $field;
            }
        }

        return null;
    }

    public function findFilter($name)
    {
        foreach ($this->filters as $filter) {
            if (strcasecmp($name, $filter->getName()) == 0) {
                return $filter;
            }
        }

        return null;
    }

    public function getDefiningClass()
    {
        return $this->class;
    }

    public function getFields()
    {
        return $this->fields;
    }

    public function getFilters()
    {
        return $this->filters;
    }

    public function getName()
    {
        return $this->name;
    }

    public static function create($name, $class)
    {
        return new Mapping($name, $class);
    }
}
