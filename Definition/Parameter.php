<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Definition;

class Parameter
{

    private $types;

    private $typeFriendly;

    private $description;

    private $defaultValue;

    private $name;

    private $required;

    public function __construct($name, $type, $defaultValue, $description, $required = false)
    {
        $this->name         = $name;
        $this->typeFriendly = $type;
        $this->types        = explode('|', $type);
        $this->defaultValue = $defaultValue;
        $this->description  = $description;
        $this->required     = $required;
    }

    public function acceptAnyValue()
    {
        return in_array('mixed', $this->types);
    }

    public function expects($type)
    {
        return in_array($type, $this->types);
    }

    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getType()
    {
        return $this->typeFriendly;
    }

    public function getTypeFriendly()
    {
        return $this->typeFriendly;
    }

    public function getValidatorPattern($full = true)
    {
        $patterns = array();

        foreach ($this->types as $type) {
            switch ($type) {
                case 'date':
                    $patterns[] = '[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])';
                    break;

                case 'int':
                    $patterns[] = '\d+';
                    break;
                /*case 'bool':
                    $patterns[] = '1|0';
                    break;*/

                case 'string':
                    $patterns[] = '\w+';
                    break;

                case 'array':
                    break;

                case 'uuid':
                    $patterns[] = '.*\-.*\-.*\-.*\-.*';
                    break;

                case 'slash':
                    $patterns[] = '.+';
                    break;

                default:
                    throw new \Exception(sprintf('Unsupported resource parameter type \'%s\' for \'%s\'', $type, $this->getName()));
                    break;
            }
        }

        if ($full == true) {
            return sprintf('/%s/', join('|', $patterns));
        } else {
            return join('|', $patterns);
        }
    }

    public function isRequired()
    {
        return $this->required;
    }
}
