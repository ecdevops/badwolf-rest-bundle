<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnauthorizedHttpException;

use Doctrine\ODM\PHPCR\Query\QueryBuilder;

abstract class Endpoint extends ContainerAware
{

    private $context;

    public function __construct(Context $initialContext = null)
    {
        $this->context = $initialContext;
    }

    public abstract function createDefinition();

    public abstract function createExportMapping();

    /**
     * Creates a query builder that is bound to the target class.
     *
     * Optionally, we apply the filters from mapping data.
     *
     * @param string $applyFilters
     *
     * @return \ModelCriteria
     */
    protected function createQueryBuilder($applyFilters = false, $applyLimits = true)
    {
        return $this->createQueryBuilderFor($this->getContentClass(), $applyFilters, $applyLimits);
    }

    protected function createQueryBuilderFor($class, $applyFilters = false, $applyLimits = true)
    {
        $queryBuilder = \PropelQuery::from($class);

        // apply current locale (not all models handled by this class have i18n enabled)
        if (method_exists($queryBuilder, 'joinWithI18n') == true) {
            $queryBuilder->joinWithI18n($this->getRequest()->getLocale());
        }

        if ($applyFilters == true) {
            $this->applyFilters($queryBuilder, $applyLimits);
        }

        return $queryBuilder;
    }

    /**
     * Aborts the request and sends the given message and HTTP status code
     * to the client.
     *
     * @param string $message Message to pass to the client.
     * @param integer $statusCode HTTP status code.
     *
     * @return Response
     */
    public function abort($message, $statusCode)
    {
        switch ($statusCode) {
            case 400:    throw new BadRequestHttpException($message);
            case 401:    throw new UnauthorizedHttpException(rand(), $message);
            case 404:    throw new NotFoundHttpException($message);
            case 409:    throw new ConflictHttpException($message);
        }

        throw new \Exception('Unknown status code, perhaps this needs to be added to abort()');
    }

    public function allowPrettyInterface()
    {
        return true;
    }

    /**
     * Applies filters from mapping data to the given query builder.
     *
     * @param QueryBuilder|Criteria $queryBuilder
     * @param boolean $applyLimits Apply offset/limit?
     *
     * @return QueryBuilder|Criteria
     */
    public function applyFilters($queryBuilder, $applyLimits = true)
    {
        if ($applyLimits == true) {
            if (get_class($queryBuilder) == 'Doctrine\ODM\PHPCR\Query\QueryBuilder') {
                $queryBuilder
                    ->setMaxResults($this->getLimit())
                    ->setFirstResult($this->getOffset());
            } else if (is_subclass_of($queryBuilder, 'ModelCriteria') == true) {
                $queryBuilder
                    ->setLimit($this->getLimit())
                    ->setOffset($this->getOffset());
            }
        }

        $mapping = $this->getExportMapping();

        foreach ($mapping->getFilters() as $filter) {
            // @todo: security
            /*if ($app['security']->isGranted($filter->getRequiredPermission()) == false) {
                continue;
            }*/

            if (($value = $this->getFilter($filter->getName())) !== null) {
                if ($value == 'null') {
                    $value = null;
                }

                $callable = $filter->getCallable();

                if ($callable !== null) {
                    $queryBuilder->$callable($value);
                }
            }
        }

        return $queryBuilder;
    }

    public function done(Result $result, $statusCode = 200, $headers = [])
    {
        return new JsonResponse($result, $statusCode, $headers);
    }

    public function getContext()
    {
        return $this->context;
    }

    public function getFilter($name)
    {
        if ($this->hasAccessToFilter($name) == false) {
            return null;
        }

        return $this->getContext()->getFilter($name);
    }

    public function getLocale()
    {
        return $this->container->get('request')->getLocale();
    }

    public function setFilter($name, $value)
    {
        if ($this->hasAccessToFilter($name) == false) {
            return null;
        }

        return $this->getContext()->setFilter($name, $value);
    }

    public final function getDefinition()
    {
        return $this->createDefinition();
    }

    public final function getExportMapping()
    {
        return $this->createExportMapping();
    }

    public function getLimit()
    {
        return $this->getContext()->getLimit();
    }

    /***
     * Gets the mapping model used specifically for create operations.
     */
    public function getCreateModel()
    {
        return $this->getModel();
    }

    /***
     * Gets the mapping model used specifically for update operations.
     */
    public function getUpdateModel()
    {
        return $this->getModel();
    }

    /**
     * Gets the mapping model to use for update/create/delete operations.
     *
     * @return Mapping
     */
    public function getModel()
    {
        return $this->getExportMapping();
    }

    public function getDocumentManager()
    {
        return $this->container->get('doctrine_phpcr.odm.default_document_manager');
    }

    public function getOffset()
    {
        return $this->getContext()->getOffset();
    }

    public function getParameter($key)
    {
        return $this->getContext()->getParameter($key);
    }

    /**
     * Shortcut to return the request service.
     *
     * @return Request
     */
    public function getRequest()
    {
        return $this->container->get('request');
    }

    /**
     * Gets the current security token.
     *
     * @return Symfony\Component\Security\Core\Authentication\Token\TokenInterface
     */
    public function getSecurityToken()
    {
        return $this->getService('security.context')->getToken();
    }

    /**
     * Gets a service by id.
     *
     * @param string $id The service id
     *
     * @return object The service
     */
    public function getService($id)
    {
        return $this->container->get($id);
    }

    /**
     * Gets the current user.
     *
     * @return Symfony\Component\Security\Core\User\AdvancedUserInterface
     */
    public function getUser()
    {
        return $this->getService('security.context')->getUser();
    }

    public function hasAccessToExpansion($name)
    {
        // @todo: security
        /*if (($expansion = $this->getExportMapping()->findExpansion($name)) !== null) {
            return $app['security']->isGranted($expansion->getRequiredPermission());
        }*/
        return true;

        return false;
    }

    public function hasAccessToField($name)
    {
        // @todo: security
        /*if (($field = $this->getExportMapping()->findField($name)) !== null) {
            return $app['security']->isGranted($field->getRequiredPermission());
        }*/
        return true;

        return false;
    }

    public function hasAccessToFilter($name)
    {
        // @todo: security
        /*if (($filter = $this->getExportMapping()->findFilter($name)) !== null) {
            return $app['security']->isGranted($filter->getRequiredPermission());
        }*/

        return true;

        return false;
    }

    /**
     * Returns true if the service id is defined.
     *
     * @param string $id The service id
     *
     * @return Boolean true if the service id is defined, false otherwise
     */
    public function hasService($id)
    {
        return $this->container->has($id);
    }

    /*
     * {@inheritdoc}
    */
    public function setContainer(ContainerInterface $container = null)
    {
        parent::setContainer($container);

        $contextArgs = $this->getRequest()->get('_endpoint_context');

        $this->context = $this->getService('bad_wolf.rest')->getContext($contextArgs[0], $contextArgs[1]);
    }

    public function wantsExpansion($name)
    {
        if ($this->hasAccessToExpansion($name) == false) {
            return false;
        }

        return $this->getContext()->wantsExpansion($name);
    }

    public function wantsField($name)
    {
        if ($this->hasAccessToField($name) == false) {
            return false;
        }

        return $this->getContext()->wantsField($name);
    }

    /**
     * Creates and returns a Form instance from the type of the form.
     *
     * @param string|FormTypeInterface $type    The built type of the form
     * @param mixed                    $data    The initial data for the form
     * @param array                    $options Options for the form
     *
     * @return Form
     */
    protected function createForm($type, $data = null, array $options = array())
    {
        return $this->container->get('form.factory')->create($type, $data, $options);
    }
}
