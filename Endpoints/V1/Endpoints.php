<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Endpoints\V1;

use Symfony\Component\HttpFoundation\Request;

use BadWolf\Bundle\RestBundle\Definition\Definition;
use BadWolf\Bundle\RestBundle\Endpoint;
use BadWolf\Bundle\RestBundle\Result;

class Endpoints extends Endpoint
{

    public function createExportMapping()
    {
        return Definition::getExportMapping();
    }

    public function createDefinition()
    {
        $def = Definition::create('Endpoints', $this->getContext())
                ->setSummary('Lists all endpoints available through the system.');

        $def->addAction('Collection', '/endpoints', 'get', 'read');

        return $def;
    }

    public function read(Request $request)
    {
        $result = new Result();
        $result->total = 0;

        // we don't support seeking so whenever we get offset > 0 return empty
        if ($this->getOffset() == 0) {
            $routerService = $this->getService('router');
            $restService   = $this->getService('bad_wolf.rest');
            $endpoints     = [];

            foreach ($routerService->getRouteCollection()->all() as $route) {
                if (is_subclass_of($route->getDefault('_endpoint_class'), 'BadWolf\Bundle\RestBundle\Endpoint') == true) {
                    $class       = $route->getDefault('_endpoint_class');
                    $contextArgs = $route->getDefault('_endpoint_context');

                    if (array_key_exists($class, $endpoints) == false) {
                        $endpoints[$class] = new $class($restService->getContext($contextArgs[0], $contextArgs[1]));
                    }
                }
            }

            foreach ($endpoints as $endpoint) {
                $def     = $endpoint->getDefinition();
                $actions = $def->getActions();

                // does the user have access to any of the actions in this resource?
                $hasAccess = false;

                foreach ($actions as $action) {
                    // @todo: security
                    /*if ($app['security']->isGranted($action->getRequiredPermission()) == true) {
                        $hasAccess = true;
                        break;
                    }*/
                }

                /*if ($hasAccess == false) {
                    continue;
                }*/

                // apply href filter against href_collection
                if (($filter = $this->getFilter('href')) !== null) {
                    $found = false;

                    foreach ($actions as $action) {
                        // little bit wrong but its just here to make the API frontend work
                        if (mb_stristr($filter, $action->getUrl()) !== false) {
                            $found = true;
                        }
                    }

                    if ($found == false) {
                        continue;
                    }
                }

                $e = $def->export($this->getContext());

                if ($this->wantsField('provider_name') == true) {
                    $e['provider_name'] = $endpoint->getContext()->getName();
                }

                if ($this->wantsField('provider_tag') == true) {
                    $e['provider_tag'] = $endpoint->getContext()->getTag();
                }

                // actions and mapping are little more than hacks for the API
                if ($this->wantsExpansion('actions') == true) {
                    $e['actions'] = [];

                    foreach ($actions as $action) {
                        // add permission requirements to the route if there are any
                        // @todo: security
                        /*if ($app['security']->isGranted($action->getRequiredPermission()) == false) {
                            continue;
                        }*/

                        $tmp = $action->export($this->getContext());

                        // if($this->wantsField('tokens') == true)
                        {
                            $tmp['tokens'] = [];

                            foreach ($action->getTokens() as $token) {
                                $tmp['tokens'][] = [
                                    'default_value' => $token->getDefaultValue(),
                                    'description'   => $token->getDescription(),
                                    'name'          => $token->getName(),
                                    'types'         => $token->getTypeFriendly()
                                ];
                            }
                        }

                        // if($this->wantsField('model') == true)
                        {
                            $tmp['model'] = [];

                            foreach ($action->getInputs() as $input) {
                                $tmp['model'][] = [
                                    'default_value' => $input->getDefaultValue(),
                                    'description'   => $input->getDescription(),
                                    'name'          => $input->getName(),
                                    'is_required'   => $input->isRequired(),
                                    'types'         => $input->getTypeFriendly()
                                ];
                            }
                        }

                        $e['actions'][] = $tmp;
                    }
                }

                if ($this->wantsExpansion('mapping') == true) {
                    $e['mapping'] = [
                        'fields'  => [],
                        'filters' => []
                    ];

                    $mapping = $endpoint->getExportMapping();

                    if ($mapping !== null) {
                        foreach ($mapping->getFields() as $field) {
                            // TODO: security
                            /*if ($app['security']->isGranted($field->getRequiredPermission()) == false) {
                                continue;
                            }*/

                            $e['mapping']['fields'][] = [
                                'description'         => $field->getDescription(),
                                'name'                => $field->getName(),
                                'required_permission' => $field->getRequiredPermission(),
                                'type'                => $field->getType()
                            ];
                        }

                        foreach ($mapping->getFilters() as $filter) {
                            // TODO: security
                            /*if ($app['security']->isGranted($filter->getRequiredPermission()) == false) {
                                continue;
                            }*/

                            $e['mapping']['filters'][] = [
                                'description' => $filter->getDescription(),
                                'name'        => $filter->getName(),
                                'type'        => $filter->getType()
                            ];
                        }
                    }
                }

                $result->data[] = $e;
            }
        }

        $result->count = $result->total = sizeof($result->data);

        return $this->done($result);
    }
}