<?php
namespace BadWolf\Bundle\RestBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

use BadWolf\Bundle\RestBundle\Service;

class ContextResetListener
{

    private $restService;

    public function __construct(Service $restService)
    {
        $this->restService = $restService;
    }

    public function onKernelRequest(GetResponseEvent $e)
    {
        $this->restService->resetContexts();
    }
}