<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Callback;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use BadWolf\Bundle\RestBundle\Definition\Field;
use BadWolf\Bundle\RestBundle\Definition\Mapping;

/**
 * Provides extra functionality when writing forms that are used as part of
 * the validation process in REST actions.
 */
abstract class AbstractType extends \Symfony\Component\Form\AbstractType
{

    private $mapping;

    /**
     * Initializes a new instance of AbstractType.
     *
     * @param Mapping $mapping
     *            Mapping data to use when building the form.
     */
    public function __construct(Mapping $mapping = null)
    {
        $this->mapping = $mapping;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        if ($this->mapping !== null) {
            $this->buildFromMapping($builder, $this->mapping);
        }
    }

    /**
     * Populates a symfony form from a mapping declaration.
     */
    public function buildFromMapping(FormBuilderInterface $builder, Mapping $mapping)
    {
        foreach ($mapping->getFields() as $field) {
            if ($field->isModel() == false) {
                continue;
            }

            $options = ['constraints' => []];

            if ($field->isRequired() == true) {
                $options['constraints'][] = new NotBlank();
            }

            $this->addFieldToFormBuilder($builder, $field, $options);
        }
    }

    /**
     * Maps the given data-type to a form field type.
     *
     * @param string $dataType
     */
    private function addFieldToFormBuilder(FormBuilderInterface $builder, Field $field, array &$options)
    {
        $type = 'unknown';

        switch (mb_strtolower($field->getType())) {
            case 'string':
            case 'int':
            case 'int[]':
            case 'float':
            case 'float[]':
            case 'string[]':
            case 'date':
                $type = 'text';
                break;

            case 'bool':
                $type = 'checkbox';
                break;
        }

        if (($parameters = $field->getValidationParameters()) !== null) {
            switch($parameters['type']) {
                case 'choice':
                    $options['constraints'][] = new Choice($parameters['options']);
                    break;
                case 'text':
                    if ((array_key_exists('options', $parameters) === true) && (array_key_exists('methods', $parameters['options']) === true)) {
                        $options['constraints'][] = new Callback($parameters['options']);
                    }
                    break;
            }
        }

        $builder->add($field->getName(), $type, $options);
    }

        /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
        ]);
    }
}
