<?php
namespace BadWolf\Bundle\RestBundle;

class Helper
{

    public static function convertArrayValuesTo(array &$arr, $type)
    {
        foreach ($arr as $k => $v) {
            settype($arr[$k], $type);
        }
    }
}