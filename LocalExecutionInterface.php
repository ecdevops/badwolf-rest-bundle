<?php
namespace BadWolf\Bundle\RestBundle;

interface LocalExecutionInterface
{

    public function execute($url, $method = 'get', $data = [], &$statusCode = null);
}