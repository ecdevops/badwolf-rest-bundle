<?php
namespace BadWolf\Bundle\RestBundle;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

class LocalExecutionService implements LocalExecutionInterface
{

    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function execute($url, $method = 'get', $data = [], &$statusCode = null)
    {
        $parentRequest = $this->container->get('request');

        if (mb_substr($url, 0, 4) === 'http') {
            $response = $this->performRemoteRequest($parentRequest, $url, $method, $data);
        } else {
            $response = $this->performLocalRequest($parentRequest, $url, $method, $data);
        }

        return json_decode($response);
    }

    private function performLocalRequest(Request $parentRequest = null, $url, $method, $data)
    {
        $urlInfo = parse_url($url);

        if ((array_key_exists('query', $urlInfo) == true) && (mb_strlen($urlInfo['query']) > 0)) {
            mb_parse_str($urlInfo['query'], $_GET);
        }

        // create the request object
        $cookies = $parentRequest ? $parentRequest->cookies->all() : [];
        $server = $parentRequest ? $parentRequest->server->all() : [];
        $request = Request::create($url, $method, $data, $cookies, [], $server, http_build_query($data));

        if ($parentRequest !== null) {
            $locale = $parentRequest->getLocale();

            $request->setSession($parentRequest->getSession());
            $request->setLocale($locale);

            $request->headers->set('Accept-Language', [$locale]);
        }

        // execute the request
        // TODO: handle errors gracefully
        return $this->container->get('kernel')
                    ->handle($request, HttpKernelInterface::SUB_REQUEST)
                    ->getContent()
                    ;
    }

    private function performRemoteRequest(Request $parentRequest, $url, $method, $data)
    {
        // TODO: error handling
        $c = curl_init($url);

        switch (mb_strtolower($method)) {
            case 'get':
                curl_setopt($c, CURLOPT_HTTPGET, true);
                break;
            case 'put':
                throw new \Exception('We don\'t support this method for local requests yet.');
                break;
        };

        $options = [
            'Content-Type: application/json'
        ];

        if ($parentRequest !== null) {
            $options[] = 'Accept-Language: ' . $parentRequest->getLocale('_locale');
        }

        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_HTTPHEADER, $options);

        $response = curl_exec($c);
        curl_close($c);

        return $response;
    }
}
