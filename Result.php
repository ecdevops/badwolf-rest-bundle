<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle;

use Symfony\Component\Form\FormInterface;

class Result
{

    /**
     * Data pertaining to the result of this endpoint.
     *
     * @var array|object
     */
    public $data;

    /**
     * List of high-level messages (warnings, errors) about the result of this endpoint.
     *
     * @var array
     */
    public $messages = [];

    /**
     * Adds form data to the result.
     */
    public function addForm(FormInterface $form)
    {
        $this->validation = [
            'is_valid' => $form->isValid(),
            'errors'   => []
        ];

        $this->getErrorMessages($form);
    }

    public function addMessage($type, $message)
    {
        $this->messages[] = [
            'type'    => $type,
            'message' => $message
        ];
    }

    /**
     * @param \Symfony\Component\Form\Form $form
     *
     * @return array
     */
    private function getErrorMessages(FormInterface $form)
    {
        if ($form->count() > 0) {
            foreach ($form->all() as $child) {
                if ($child->isValid() == false) {
                    $this->getErrorMessages($child);
                }
            }
        } else {
            foreach ($form->getErrors() as $error) {
                $this->validation['errors'][$form->getName()] = $error->getMessage();
            }
        }
    }
}