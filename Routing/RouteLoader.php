<?php
namespace BadWolf\Bundle\RestBundle\Routing;

use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

use BadWolf\Bundle\RestBundle\Service;

class RouteLoader extends Loader
{
    private $restService;

    public function __construct(Service $service)
    {
        $this->restService = $service;
    }

    public function supports($resource, $type = null)
    {
        return 'bad_wolf_endpoint' === $type;
    }

    public function load($resource, $type = null)
    {
        return $this->restService->getRoutes();
    }
}