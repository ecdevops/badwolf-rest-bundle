<?php
/* ==========================================================================
 * Copyright (c) 2013 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

class Service
{

    private $routes;

    private $exportables = [];

    private $contexts = [];

    public function __construct()
    {
        $this->routes = new RouteCollection();
    }

    public function addExportable($class)
    {
        $this->exportables[] = $class;
    }

    public function addEndpoint($version, $group, $class)
    {
        $obj = new $class($this->getContext($version, $group));
        $def = $obj->getDefinition();

        foreach ($def->getActions() as $action) {
            if (($href = $action->getUrl()) === null) {
                continue;
            }

            $defaults = [
                '_controller' => sprintf('%s::%s', $class, $action->getCallable()),
                '_format' => 'json',
                '_endpoint_class' => $class,
                '_endpoint_context' => [$version, $group]
            ];

            $route = new Route($href, $defaults, [], [], '', [], $action->getMethod());
            $routeName = sprintf('bad_wolf_rest__%s_%s', mb_ereg_replace('[\{\}\-\/]', '_', $action->getUrl()), $action->getMethod());

            // @todo: permissions
            // add permission requirements to the route if there are any
            /*if (sizeof($action['required_permission']) > 0) {
                $controller->secure($action['required_permission']);
            }*/

            $this->routes->add($routeName, $route);

           /* $actionCacheEntry = [
                '_rest_callable'      => $action->getCallable(),
                'href'                => $href,
                'method'              => $action->getMethod(),
                'required_permission' => $action->getRequiredPermission(),
                'tokens'              => []
            ];

            // add constraints and validators to the cache
            foreach ($action->getTokens() as $token) {
                $actionCacheEntry['tokens'][] = [
                    'name'              => $token->getName(),
                    'default_value'     => $token->getDefaultValue(),
                    'validator_pattern' => $token->acceptAnyValue() ? null : $token->getValidatorPattern(false)
                ];
            }

            $endpointCacheEntry['actions'][] = $actionCacheEntry;*/
        }

        //$this->cache[] = $endpointCacheEntry;*/


    }

    public function getExportables()
    {
        return $this->exportables;
    }

    public function getContext($version, $group)
    {
        $contextKey = sprintf('%s_%s', $version, $group);

        if (array_key_exists($contextKey, $this->contexts) == true) {
            return $this->contexts[$contextKey];
        }

        return ($this->contexts[$contextKey] = new Context($version, $group, $group));
    }

    /**
     * Gets the list of routes that have been generated from
     * the endoint declarations.
     *
     * @return \Symfony\Component\Routing\RouteCollection
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * Rests all contexts causing them to re-evaluate their environments.
     */
    public function resetContexts()
    {
        foreach ($this->contexts as $context) {
            $context->reset();
        }
    }
}
