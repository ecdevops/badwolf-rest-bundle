<?php
namespace BadWolf\Bundle\RestBundle\Templating;

use BadWolf\Bundle\RestBundle\LocalExecutionInterface;

class TwigExtension extends \Twig_Extension
{

    private $restInterface;

    public function __construct(LocalExecutionInterface $restInterface)
    {
        $this->restInterface = $restInterface;
    }

    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return [
            'rest' => new \Twig_SimpleFunction('rest', [$this, 'functionRest'])
        ];
    }

    public function getName()
    {
        return 'rest_bundle';
    }

    /**
     * Executes a REST request.
     * 
     * @param string $url        URL to call.
     * @param boolean $firstOnly For collections only return the first result.
     * 
     * @return array|null
     */
    public function functionRest($url, $firstOnly = false)
    {
        $result = $this->restInterface->execute($url);

        if ($firstOnly === true) {
            if ($result->count > 0) {
                return $result->data[0];
            }

            return null;
        } else {
            return $result;
        }
    }
}
