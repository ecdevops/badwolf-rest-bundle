<?php
/* ==========================================================================
 * Copyright (c) 2012 EC Holdings Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to permit
 * persons to whom the Software is furnished to do so, subject to the
 * following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
 * NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
 * OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
 * USE OR OTHER DEALINGS IN THE SOFTWARE.
 * ==========================================================================
 */
namespace BadWolf\Bundle\RestBundle\Traits;

use BadWolf\Bundle\RestBundle\Context;
use BadWolf\Bundle\RestBundle\Definition\Mapping;

trait ExportTrait
{

    private $exportCache = null;

    public function export(Context $context = null, $expand = true, $alternateMapping = null, $forceAllFields = false)
    {
        if (($this->exportCache === null) || ($this->exportCache != $alternateMapping)) {
            $this->exportCache = ($alternateMapping !== null) ? $alternateMapping : $this->getExportMapping();
        }

        return $this->exportByMapping($context, $expand, $this->exportCache, $forceAllFields);
    }

    public function exportAll(Context $context = null, $expand = true, $alternateMapping = null)
    {
        return $this->export($context, $expand, $alternateMapping, true);
    }

    private function exportByMapping(Context $context = null, $expand, Mapping $mapping, $forceAllFields)
    {
        $e = [];

        // add href if we have a context
        if ($context !== null) {
            if (($href = $this->getHref($context)) !== null) {
                $e['href'] = $href;
            }
        }

        if ($expand == true) {
            foreach ($mapping->getFields() as $def) {
                // a null context means all fields exception expansions
                if (($context === null) || ($forceAllFields == true) || ($context->wantsField($def->getName()) == true)) {
                    // do they have permission to get this field?
                    // @todo: security
                    /*if ($app['security']->isGranted($def->getRequiredPermission()) == false) {
                        continue;
                    }*/

                    $callable = $def->getGetter();

                    if ($callable !== null) {
                        if (is_array($callable) == true) {
                            $val = call_user_func($callable, $this);
                        } else {
                            $val = $this->$callable();
                        }

                        $e[$def->getName()] = $this->exportValue($context, $val);
                    }
                }
            }
        }

        return $e;
    }

    private function exportValue(Context $context = null, $value)
    {
        $return = null;

        if ((is_array($value) === true) || ($value instanceof \ArrayObject)) {
            $return = [];

            foreach ($value as $key => $otherValue) {
                $return[$key] = $this->exportValue($context, $otherValue);
            }
        } else if ($value instanceof \DateTime) {
            $return = $value->format(\DateTime::ISO8601);
        } else if (is_object($value) == true) {
            $return = $value->export($context);
        } else {
            $return = $value;
        }

        return $return;
    }

    public function getExportMapping()
    {
        return new Mapping('<not filled in>');
    }

    public abstract function getHref(Context $context);
}
